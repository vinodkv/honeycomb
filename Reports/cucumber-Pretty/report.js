$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("tm_favorite.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: rchandra@altimetrik.com"
    }
  ],
  "line": 3,
  "name": "Different Operation on Favrorite API",
  "description": "",
  "id": "different-operation-on-favrorite-api",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 7,
  "name": "Favoriting an Event",
  "description": "",
  "id": "different-operation-on-favrorite-api;favoriting-an-event",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "make a request with post method with \"\u003centityid\u003e\" and \"\u003cdevicetype\u003e\" and \"\u003csource\u003e\" and \"\u003chmacid\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "verify the response code should be \"\u003cresponsecode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "verify the favouriteid",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "launch webApplication with url \"\u003curl\u003e\" and browser \"\u003cbrowser\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 14,
  "name": "",
  "description": "",
  "id": "different-operation-on-favrorite-api;favoriting-an-event;",
  "rows": [
    {
      "cells": [
        "entityid",
        "devicetype",
        "source",
        "hmacid",
        "responsecode",
        "url",
        "browser"
      ],
      "line": 16,
      "id": "different-operation-on-favrorite-api;favoriting-an-event;;1"
    },
    {
      "cells": [
        "K8vZ9171Jo7",
        "DESKTOP",
        "TMAPP",
        "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
        "200",
        "https://ticketmaster.com",
        "firefox"
      ],
      "line": 17,
      "id": "different-operation-on-favrorite-api;favoriting-an-event;;2"
    },
    {
      "cells": [
        "K8vZ9171Jo7",
        "DESKTOP",
        "TMAPP",
        "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
        "200",
        "https://ticketmaster.com",
        "chrome"
      ],
      "line": 18,
      "id": "different-operation-on-favrorite-api;favoriting-an-event;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 17,
  "name": "Favoriting an Event",
  "description": "",
  "id": "different-operation-on-favrorite-api;favoriting-an-event;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "make a request with post method with \"K8vZ9171Jo7\" and \"DESKTOP\" and \"TMAPP\" and \"cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "verify the response code should be \"200\"",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "verify the favouriteid",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "launch webApplication with url \"https://ticketmaster.com\" and browser \"firefox\"",
  "matchedColumns": [
    5,
    6
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "K8vZ9171Jo7",
      "offset": 38
    },
    {
      "val": "DESKTOP",
      "offset": 56
    },
    {
      "val": "TMAPP",
      "offset": 70
    },
    {
      "val": "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
      "offset": 82
    }
  ],
  "location": "TM_Favorite.make_a_request_with_post_method_with_and_and_and(String,String,String,String)"
});
formatter.result({
  "duration": 3595069710,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 36
    }
  ],
  "location": "TM_Favorite.verify_the_response_code_should_be(String)"
});
formatter.result({
  "duration": 1665907,
  "status": "passed"
});
formatter.match({
  "location": "TM_Favorite.verify_the_favouriteid()"
});
formatter.result({
  "duration": 8802303,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://ticketmaster.com",
      "offset": 32
    },
    {
      "val": "firefox",
      "offset": 71
    }
  ],
  "location": "TM_Favorite.launch_webApplication_with_url_and_browser(String,String)"
});
formatter.result({
  "duration": 1255306394,
  "error_message": "org.openqa.selenium.WebDriverException: Cannot find firefox binary in PATH. Make sure firefox is installed. OS appears to be: MAC\nBuild info: version: \u00273.10.0\u0027, revision: \u0027176b4a9\u0027, time: \u00272018-03-02T19:03:16.397Z\u0027\nSystem info: host: \u0027AIPL-LT-1175.local\u0027, ip: \u00272401:4900:2500:9ca9:989c:57e3:6280:2%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.14.6\u0027, java.version: \u00271.8.0_231\u0027\nDriver info: driver.version: FirefoxDriver\n\tat org.openqa.selenium.firefox.FirefoxBinary.\u003cinit\u003e(FirefoxBinary.java:100)\n\tat java.util.Optional.orElseGet(Optional.java:267)\n\tat org.openqa.selenium.firefox.FirefoxOptions.getBinary(FirefoxOptions.java:216)\n\tat org.openqa.selenium.firefox.FirefoxDriver.toExecutor(FirefoxDriver.java:187)\n\tat org.openqa.selenium.firefox.FirefoxDriver.\u003cinit\u003e(FirefoxDriver.java:147)\n\tat org.openqa.selenium.firefox.FirefoxDriver.\u003cinit\u003e(FirefoxDriver.java:125)\n\tat com.alti.base.Driverutils.initWebDriver(Driverutils.java:73)\n\tat com.alti.base.Driverutils.launchApplication(Driverutils.java:33)\n\tat stepsDefinitions.TM_Favorite.launch_webApplication_with_url_and_browser(TM_Favorite.java:93)\n\tat ✽.Then launch webApplication with url \"https://ticketmaster.com\" and browser \"firefox\"(tm_favorite.feature:11)\n",
  "status": "failed"
});
formatter.scenario({
  "line": 18,
  "name": "Favoriting an Event",
  "description": "",
  "id": "different-operation-on-favrorite-api;favoriting-an-event;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "make a request with post method with \"K8vZ9171Jo7\" and \"DESKTOP\" and \"TMAPP\" and \"cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "verify the response code should be \"200\"",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "verify the favouriteid",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "launch webApplication with url \"https://ticketmaster.com\" and browser \"chrome\"",
  "matchedColumns": [
    5,
    6
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "K8vZ9171Jo7",
      "offset": 38
    },
    {
      "val": "DESKTOP",
      "offset": 56
    },
    {
      "val": "TMAPP",
      "offset": 70
    },
    {
      "val": "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
      "offset": 82
    }
  ],
  "location": "TM_Favorite.make_a_request_with_post_method_with_and_and_and(String,String,String,String)"
});
formatter.result({
  "duration": 1230875559,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 36
    }
  ],
  "location": "TM_Favorite.verify_the_response_code_should_be(String)"
});
formatter.result({
  "duration": 89916,
  "status": "passed"
});
formatter.match({
  "location": "TM_Favorite.verify_the_favouriteid()"
});
formatter.result({
  "duration": 7958115,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://ticketmaster.com",
      "offset": 32
    },
    {
      "val": "chrome",
      "offset": 71
    }
  ],
  "location": "TM_Favorite.launch_webApplication_with_url_and_browser(String,String)"
});
formatter.result({
  "duration": 8411781806,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "list all the favorite from my favorite page",
  "description": "",
  "id": "different-operation-on-favrorite-api;list-all-the-favorite-from-my-favorite-page",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 22,
  "name": "make an GET Api Request",
  "keyword": "Given "
});
formatter.step({
  "line": 23,
  "name": "List all the events",
  "keyword": "Then "
});
formatter.match({
  "location": "TM_Favorite.make_an_GET_Api_Request()"
});
formatter.result({
  "duration": 1342052340,
  "status": "passed"
});
formatter.match({
  "location": "TM_Favorite.list_all_the_events()"
});
formatter.result({
  "duration": 31539722,
  "status": "passed"
});
});
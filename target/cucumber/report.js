$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("favoriteapi.feature");
formatter.feature({
  "line": 1,
  "name": "Operations on Favorite API",
  "description": "",
  "id": "operations-on-favorite-api",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "do Favorite",
  "description": "",
  "id": "operations-on-favorite-api;do-favorite",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "make a request with post method with \"\u003centityid\u003e\" and \"\u003cdevicetype\u003e\" and \"\u003csource\u003e\" and \"\u003chmacid\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "verify the response code should be \"\u003cresponsecode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "verify the favouriteid",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "launch webApplication with \"\u003curl\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 10,
  "name": "",
  "description": "",
  "id": "operations-on-favorite-api;do-favorite;",
  "rows": [
    {
      "cells": [
        "entityid",
        "devicetype",
        "source",
        "hmacid",
        "responsecode",
        "url",
        "browser"
      ],
      "line": 12,
      "id": "operations-on-favorite-api;do-favorite;;1"
    },
    {
      "cells": [
        "K8vZ9171Jo7",
        "DESKTOP",
        "TMAPP",
        "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
        "200",
        "https://ticketmaster.com",
        "chrome"
      ],
      "line": 13,
      "id": "operations-on-favorite-api;do-favorite;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 13,
  "name": "do Favorite",
  "description": "",
  "id": "operations-on-favorite-api;do-favorite;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "make a request with post method with \"K8vZ9171Jo7\" and \"DESKTOP\" and \"TMAPP\" and \"cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "verify the response code should be \"200\"",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "verify the favouriteid",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "launch webApplication with \"https://ticketmaster.com\"",
  "matchedColumns": [
    5
  ],
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 15,
  "name": "list all the favorite from my favorite page",
  "description": "",
  "id": "operations-on-favorite-api;list-all-the-favorite-from-my-favorite-page",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 16,
  "name": "make an GET Api Request",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "List all the events",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});
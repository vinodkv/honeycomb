package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.log4testng.Logger;

public class HomePage {
Logger log = Logger.getLogger(HomePage.class);

public HomePage(WebDriver driver) {
	PageFactory.initElements(driver, this);
}

@FindBy(css="button div svg g path[fill='currentColor']")
private WebElement login_btn;


public void clickOn_loginBtn() {
	login_btn.click();
}

}

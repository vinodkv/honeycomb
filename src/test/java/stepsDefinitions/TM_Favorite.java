package stepsDefinitions;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.ITestResult;

import com.alti.base.BrowserService;
import com.alti.base.Driverutils;
import com.alti.common.ConfigReader;
import com.alti.common.RestUtilities;
import com.alti.common.Specifications;
import com.alti.constants.EndPoints;
import com.alti.constants.Paths;
import com.alti.reporter.ExtentManager;
import com.alti.reporter.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;

import PageObjects.HomePage;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;


public class TM_Favorite extends BrowserService{
	
	
	
	Logger logger = Logger.getLogger(TM_Favorite.class);
	Response res;
	JsonPath jsonpath;
	
	@Given("^make a request with post method with \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void make_a_request_with_post_method_with_and_and_and(String entityid, String devicetype, String source, String hmacid) throws Throwable {
		logger.info("Test Started");
		res = given()
		.contentType(ContentType.JSON)
		.body("{\"operationName\":\"OnboardingPushFavorite\",\"variables\":{\"input\":{\"Entity_Id\":\""+entityid+"\",\"Device_Type\":\""+devicetype+"\",\"Device_Id\":null,\"Entity_Id_Source\":\"DISCOVERY\",\"Entity_Id_Type\":\"ATTRACTION\",\"Type\":\"FAV\",\"Source\":\"TMAPP\",\"Customer_Id\":\"cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7\",\"Hmac_Id\":\"cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7\",\"Legacy_Id\":\"1094215\"}},\"extensions\":{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"bba28e2776cfe34932008b68547b5390257a0f1cc2332ebabdfe0867c89979e6\"}}}")
		.when().post("https://beta.tmol.co/api/next/graphql/")
		.then().log().all().extract().response();
		 jsonpath = RestUtilities.getJsonPath(res);
		System.out.println("status Code "+ res.getStatusCode());
		System.out.println("Favorite ID " + jsonpath.getString("data.trackingFavoritePush.Favorite_Id"));
		//ExtentTestManager.getTest().log(LogStatus.INFO, "Favorite ID verified successfully");
	}

	@Then("^verify the response code should be \"([^\"]*)\"$")
	public void verify_the_response_code_should_be(String respcode) throws Throwable {
		System.out.println("@Then : " + res.getStatusCode());
		Assert.assertEquals(res.getStatusCode(), Integer.parseInt(respcode));
	}

	@Then("^verify the favouriteid$")
	public void verify_the_favouriteid() throws Throwable {
	    jsonpath.getString("data.trackingFavoritePush.Favorite_Id");
	}

	@Given("^make an GET Api Request$")
	public void make_an_GET_Api_Request() throws Throwable {
		res = given().queryParam("operationName", "OnboardingAttractionAndVenueEvents")
		.queryParam("variables",
				"{\"discoveryAttractionsId\":\"K8vZ917KjPf,K8vZ9174Za7,K8vZ9171G-7,K8vZ9171Jo7\",\"discoveryVenuesId\":\"\",\"legacyAttractionsId\":\"\",\"legacyVenuesId\":\"\",\"loadDiscoveryAttractions\":true,\"loadDiscoveryVenues\":false,\"loadLegacyAttractions\":false,\"loadLegacyVenues\":false,\"geoHash\":null,\"sort\":\"date,asc\",\"locale\":\"en-us\",\"startDateTime\":\"2019-10-24T09:38:17+05:30\"}")
		.queryParam("extensions",
				"{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"438f449bead84b89a06fb82c1c5e91b75b7d64a5153f8382497bec62b82bd182\"}}")
		.when()
		.get("https://beta.tmol.co/api/next/graphql/").then().log().all().extract().response();
		jsonpath = RestUtilities.getJsonPath(res);
	    
	}

	@Then("^List all the events$")
	public void list_all_the_events() throws Throwable {
	    System.out.println("Attraction Name " + jsonpath.getString("data.attractions.items[0].name"));
	    logger.info("Attraction Name " + jsonpath.getString("data.attractions.items[0].name"));
	}
	
	
	
	
	@Then("^launch webApplication with url \"([^\"]*)\" and browser \"([^\"]*)\"$")
	public void launch_webApplication_with_url_and_browser(String url, String browser) throws Throwable {
	    Driverutils.launchApplication(url,browser);
	}

}

package StepsDefs;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class ApiTestdefs {
	
	@Given("^user make a get operation$")
	public void user_make_a_get_operation() throws Throwable {
		given()
		.get("https://jsonplaceholder.typicode.com/todos/2")
		.then()
		.body("title", equalTo("quis ut nam facilis et officia qui"))
		.log().all();
	}

	@Then("^user verify the response$")
	public void user_verify_the_response() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("Verification");
	}

	
	@Given("^An GET API call is performed$")
	public void an_GET_API_call_is_performed() throws Throwable {
		given().queryParam("operationName", "OnboardingAttractionAndVenueEvents")
		.queryParam("variables",
				"{\"discoveryAttractionsId\":\"K8vZ917KjPf,K8vZ9174Za7,K8vZ9171G-7,K8vZ9171Jo7\",\"discoveryVenuesId\":\"\",\"legacyAttractionsId\":\"\",\"legacyVenuesId\":\"\",\"loadDiscoveryAttractions\":true,\"loadDiscoveryVenues\":false,\"loadLegacyAttractions\":false,\"loadLegacyVenues\":false,\"geoHash\":null,\"sort\":\"date,asc\",\"locale\":\"en-us\",\"startDateTime\":\"2019-10-24T09:38:17+05:30\"}")
		.queryParam("extensions",
				"{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"438f449bead84b89a06fb82c1c5e91b75b7d64a5153f8382497bec62b82bd182\"}}")
		.when()
		.get("https://beta.tmol.co/api/next/graphql/").then().log().all();
	}

	@Then("^I verify the content of my favorite$")
	public void i_verify_the_content_of_my_favorite() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}
	
	@Given("^user makes a POST Request with \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_makes_a_POST_Request_with_and_and_and(String arg1, String arg2, String arg3, String arg4) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}



}

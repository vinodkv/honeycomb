package tests;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;

public class ApiTest {
	
	
	//@Test
	@Test
	public void testStatusCode() {
		given()
		.get("http://jsonplaceholder.typicode.com/posts/3")
		.then()
		.statusCode(200);
	}
	
	//@Test
	@Test
	public void testLogging() {
		given()
		.get("https://jsonplaceholder.typicode.com/todos/2")
		.then()
		.body("title", equalTo("quis ut nam facilis et officia qui"))
		.log().all();
	}

}
